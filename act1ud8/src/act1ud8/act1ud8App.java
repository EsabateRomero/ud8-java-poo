package act1ud8;
import java.util.Random;

import dto.Persona;
/**
 * @author Elisabet Sabat�
 *
 */
public class act1ud8App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		String nombre = "Elisabet", sexo = "mujer", dni = "48281780L";
		int edad = 23, peso = 72, altura = 172;
		
		Persona p1 = new Persona();
		System.out.println(p1);
		Persona p2 = new Persona(nombre, edad, sexo);
		System.out.println(p2);
		Persona p3 = new Persona(nombre, edad, dni, sexo, peso, altura);
		if(p3.getDni().equalsIgnoreCase(dni))
			System.out.println("\n\nRegistre actualitzat amb dni");
		else
			System.out.println("\n\nRegistre actualitzat sense dni, s'ha generat un");
		
		System.out.println(p3);
	}

}
