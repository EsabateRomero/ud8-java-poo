package dto;

import java.util.Random;

public class Persona {
	
	private final static char HOMBRE = 'H';
	private final static char MUJER = 'M';
    private final static char [] letras = {'T','R','W','A','G','M','Y','F','P','D','X','B','N','J','Z','S','Q','V','H','L','C','K','E'};
	
    private String nombre;
	private int edad;
	private String dni;
	private char sexo;
	private double peso;
	private double altura;
	
	public Persona() {
		this.nombre="";
		this.edad = 0;
		this.dni = obtenerDni();
		this.sexo = HOMBRE;
		this.peso = 0;
		this.altura = 0;
	}
	
	public Persona(String nombre, int edad, String sexo) {
		this.nombre = nombre;
		this.edad = edad;
		if(sexo.equalsIgnoreCase("hombre"))
			this.sexo = HOMBRE;
		else if(sexo.equalsIgnoreCase("mujer"))
			this.sexo = MUJER;
		
		this.dni = obtenerDni();
		this.peso = 0;
		this.altura = 0;
	}
	
	
	public Persona(String nombre, int edad, String dni, String sexo, double peso, double altura) {
		this.nombre = nombre;
		this.edad = edad;
		if(comprobarDni(dni))
			this.dni = dni;
		else
			this.dni = obtenerDni();
		if(sexo.equalsIgnoreCase("hombre"))
			this.sexo = HOMBRE;
		else if(sexo.equalsIgnoreCase("mujer"))
			this.sexo = MUJER;
		this.peso = peso;
		this.altura = altura;
	}

	public static String obtenerDni() {
		Random random = new Random();
		int contador = 0, letra;
		String dni = "";
		while(contador<8) {
			dni = dni + random.nextInt(9);
			contador++;
		}
		letra = Integer.parseInt(dni);
		letra = letra%23;
		return dni+letras[letra];
	}
	
	public static boolean comprobarDni(String dni) {
		int contador = 0, letra;
		String dni2 ="";
		
		while(contador<8) {
			dni2 = dni2 + dni.charAt(contador);
			contador++;
		}
		letra = Integer.parseInt(dni2);
		letra = letra%23;
		if((dni2+letras[letra]).equalsIgnoreCase(dni))
			return true;
		else 
			return false;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public char getSexo() {
		return sexo;
	}

	public void setSexo(char sexo) {
		this.sexo = sexo;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	@Override
	public String toString() {
		return "-----\nnom: "+nombre+"\nedad: "+edad+"\ndni: "+dni+"\nsexo: "+sexo + "\npeso: " + peso + "\naltura: "+altura;
	}
		
	
	
}
