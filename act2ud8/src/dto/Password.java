package dto;

import java.util.Random;

public class Password {
	private int longitud;
	private String contraseña;
   
	public Password() {
		this.longitud = 8;
		this.contraseña = crearContraseña(longitud);
	}
	
	public Password(int longitud) {
		this.longitud = longitud;
		this.contraseña = crearContraseña(longitud);
	}
	
	public static String crearContraseña(int longitud) {
		Random random = new Random();
		int contador = 0; String contraseña = "";
		while(contador<longitud) {
			int charValor = random.nextInt(57)+65;
			char valor = (char)(charValor);
			contraseña = contraseña + valor;
			
			contador++;
			
		}
		
		return contraseña;
	}

	@Override
	public String toString() {
		return "-----\nlongitud: " + longitud + "\nContraseña: " + contraseña;
	}
		
	
	
}
