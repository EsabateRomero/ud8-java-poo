package dto;

import java.util.Random;

public class Password {
	private char [] caracteresContraseña = {'A','B','C','D','E','F','G','H','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f','g','h','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z', (char)48, (char)49, (char)50, (char)51, (char)52, (char)53, (char)54, (char)55, (char)56, (char)57};
	
	private int longitud;
	private String contraseña;
   
	public Password() {
		this.longitud = 8;
		this.contraseña = crearContraseña(longitud, caracteresContraseña);
	}
	
	public Password(int longitud) {
		this.longitud = longitud;
		this.contraseña = crearContraseña(longitud, caracteresContraseña);
	}
	
	public static String crearContraseña(int longitud, char[] caracteres) {
		Random random = new Random();
		int contador = 0; String contraseña = "";
		while(contador<longitud) {
			int charValor = random.nextInt(caracteres.length-1);
			char valor = (caracteres[charValor]);
			contraseña = contraseña + valor;
			
			contador++;
			
		}
		
		return contraseña;
	}

	@Override
	public String toString() {
		return "-----\nlongitud: " + longitud + "\nContraseña: " + contraseña;
	}
		
	
	
}
