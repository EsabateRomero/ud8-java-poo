package act3ud8;
import java.util.Random;
import java.util.Scanner;

import dto.Electrodomestico;
/**
 * @author Elisabet Sabat�
 *
 */
public class act3ud8App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner teclat = new Scanner(System.in);
		
		//alt shift r
		System.out.println("----\nCREACI� DEL PRIMER OBJECTE AMB UN CONSTRUCTOR BUIT:");
		Electrodomestico electrodomesticBuit = new Electrodomestico();
		System.out.println(electrodomesticBuit);
		
		System.out.println("-----\nCREACI� DEL OBJECTE AMB DOS PAR�METRES DEL USUARI\n--------");
		
		int precioBase = precioBase(teclat);
		int peso = peso(teclat);
		
		Electrodomestico electrodomestico2 = new Electrodomestico(precioBase, peso);
		System.out.println(electrodomestico2);

		System.out.println("-------\nCREACI� DEL OBJECTE AMB TOTS ELS PAR�METRES");
		
		String color = color(teclat);
		char consumEnergetic = consumEnergetic(teclat);
		
		Electrodomestico electrodomesticoPle = new Electrodomestico(precioBase, peso, color, consumEnergetic);
		System.out.println(electrodomesticoPle);
		
		
	}
	
	public static char consumEnergetic(Scanner teclat) {
		char consum = 'F';
		boolean consumEnergeticValid = false;
		while(!consumEnergeticValid) {
		System.out.println("Dinos el vostre consum energ�tic (F/A):");
		String consumUsuari = teclat.next();
		if(consumUsuari.length()==1) {
			
			if(consumUsuari.equalsIgnoreCase("A")||consumUsuari.equalsIgnoreCase("F")) {
				consumUsuari = consumUsuari.toUpperCase();
				consum = consumUsuari.charAt(0);
				consumEnergeticValid = true;
			}else
				System.out.println("Heu d'introduir el valor A o F, heu introduit " + consumUsuari);
			
		}
		
		else
			System.out.println("Sisplau, heu d'introduir un valor d'un car�cter, no us exediu.");
		
		}
		return consum;
	}
	
	
	public static String color(Scanner teclat) {
		String color = "";
		boolean colorValid = false;
		while(!colorValid) {
		System.out.println("Dinos el color:");
		color = teclat.next();
		if(color.equalsIgnoreCase("blanc")||color.equalsIgnoreCase("negre")||color.equalsIgnoreCase("vermell")||color.equalsIgnoreCase("blau")||color.equalsIgnoreCase("gris"))
			colorValid = true;
		else
			System.out.println("Sisplau, heu d'introduir un valor d'entre els disponibles (blanc, negre, vermell, blau i gris).");
		
		}
		return color;
	}
	
	public static int precioBase(Scanner teclat) {
		int precioBase = 0;
		boolean precioBaseValid = false;
		while(!precioBaseValid) {
		System.out.println("Dinos el precio base:");
		precioBase = teclat.nextInt();
		if(precioBase>0)
			precioBaseValid = true;
		else
			System.out.println("Sisplau, heu d'introduir un valor positiu.");
		}
		return precioBase;
	}
	public static int peso(Scanner teclat) {
		int peso = 0;
		boolean pesoValido = false;
		while(!pesoValido) {
		System.out.println("Dinos el peso:");
		peso = teclat.nextInt();
		if(peso>0)
			pesoValido = true;
		else
			System.out.println("Sisplau, heu d'introduir un valor positiu.");
		}
		return peso;
	}

}
