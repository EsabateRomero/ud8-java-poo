package dto;

import java.util.Random;

public class Electrodomestico {
	
	private int precioBase = 100, peso = 5;
	private String color = "blanc";
	private char consumEnergetic = 'F';
	
	public Electrodomestico() {

	}
	
	public Electrodomestico(int precio, int peso) {
		this.precioBase = precio;
		this.peso = peso;
		
	}
	
	public Electrodomestico(int precioBase, int peso, String color, char consumEnergetic) {
		super();
		this.precioBase = precioBase;
		this.peso = peso;
		this.color = color;
		this.consumEnergetic = consumEnergetic;
	}

	
	@Override
	public String toString() {
		return "-----\nPrecio base: " + precioBase + "\nPeso: " +  peso + "\nConsum Energetic: " + consumEnergetic + "\nColor del electrodoméstico: " +color;
	}
		
	
	
}
