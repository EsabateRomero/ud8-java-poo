package act4ud8;
import java.util.Random;

import dto.Source;
/**
 * @author Elisabet Sabat�
 *
 */
public class act4ud8App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		String creador = "Senyor Crea-coses", titulo = "Creant un objecte", genero = "novela";
		int numTemporades = 6;
		
		Source p1 = new Source();
		System.out.println(p1);
		
		Source p2 = new Source(titulo, creador);
		System.out.println(p2);
		
		Source p3 = new Source(titulo,creador,genero,numTemporades);
		System.out.println(p3);

	}

}
