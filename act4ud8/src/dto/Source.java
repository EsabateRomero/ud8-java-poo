package dto;

import java.util.Random;

public class Source {
	
	
    private String titulo, genero, creador;
	private int numeroTemporadas = 3;
	private boolean entregado = false;

	public Source() {
		this.titulo = "";
		this.genero = "";
		this.creador = "";
	
	}
	
	public Source(String titulo, String creador) {
		this.titulo = titulo;
		this.creador = creador;
		this.genero = "";

	}

	public Source(String titulo, String genero, String creador, int numeroTemporadas) {
		super();
		this.titulo = titulo;
		this.genero = genero;
		this.creador = creador;
		this.numeroTemporadas = numeroTemporadas;
	
	}



	@Override
	public String toString() {
		return "-----\nTitulo: " + titulo + "\nCreador: " +creador +"\nGenero: " + genero + "\nNumero de Temporadas: " + numeroTemporadas + "\nEntregado: "+entregado;
	}
		
	
	
}
